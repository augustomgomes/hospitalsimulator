# Hospital simulator

### 1. Introdution ##

This product is a reponse to a hospital simulator challenge.
It is made in a way that anyone can be easily read an understand.
it was implemented a rules generator that simplify the way the patients final state calculation based on  medication is made.


It was developed an aplication , valuating teh Kiss principle , with the funcionalities:
* Predict the state of patients after the care with a list os drugs
* allow to define two strategies to determine the patient final state




### 2. Developpindg the aplication ##
	

For developping the aplication, i have created a maven project in eclipse.
I have created a main class SimulatorManager that is a singleton to avoid more than one instance of the program runnning.
I have defined  two strategies to verify the rules, using the strategy pattern: In the first one the patient input state does not change 
from rule to rule; In the second one, the patient state result of one rule execution, is the input of the next rule.
The strategy is configurable in a simulator.properties files in the resources folder.
I have used a facade pattern to isolate from the complexity of the implementation of each rule.
the results are saved in a list of final state.
at last the result are printed in the printResult method of the strategy implementation.


### Automated test ###

for test it was used JUnit for testing the rules

### Building and running the aplication ###


The build is made in eclipse or, navigating to the folder where the pom file is, and in commnad line issuing the commnand:

```
	mvn install 
```

for running the application, navigate to the target folder and run , for example, the command:


```
	java -cp simulator-0.0.1-SNAPSHOT.jar com.hospital.simulator.App "D,F,F,T", "As,I,An"
```



where the first string is the pacients input states, and the second string is the list of the drugs that each patients take.

Code can be accessed from https://bitbucket.org/augustomgomes/hospitalsimulator/

### Observations ###

I did not payed very much atention to errors and validations, But it's a situation that is usuly done in a normal situation.

### Improvements ###

It could be included a rule inference tool like Drools to infere the result from rules.
It could be implemented a thread pool with several theads to run in paralel to allow better performance. Each thread could run a taks at a time,
that could be for example running on rule execution.
