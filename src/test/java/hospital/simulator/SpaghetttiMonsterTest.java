package hospital.simulator;

import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import com.hospital.simulator.entities.Drug;
import com.hospital.simulator.process.management.SimulatorManager;
import com.hospital.simulator.rules.beans.FlyingSpaghetiMonster;

public class SpaghetttiMonsterTest {
	
	@Test
	public void testeNoodlyPower() {
		
		//scene
		long xPacientesTratados = 1000001;	
		SimulatorManager instance = SimulatorManager.getInstance();
		instance.setNumeroPacientesTratados(xPacientesTratados);
		FlyingSpaghetiMonster rule = new FlyingSpaghetiMonster();		
		List<Drug> drugs = new ArrayList<Drug>();		
		drugs.add(new Drug("As", "Drug1"));	
		String estadoInicial = "X";		
		
		//action
		String estadoFinal = rule.processState(drugs, estadoInicial);
		
		//Verificaçtion
		assertTrue(estadoFinal.compareTo("H") == 0);	
		
	}

}
