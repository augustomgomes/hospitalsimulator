package hospital.simulator;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;
import org.junit.Test;

import com.hospital.simulator.entities.Drug;
import com.hospital.simulator.rules.beans.AspirinCuresFever;

public class ApirinCuresFeverTest {
	
	@Test
	public void FeverCureTest() {

		// scene
		AspirinCuresFever rule = new AspirinCuresFever();
		List<Drug> drugs = new ArrayList<Drug>();
		drugs.add(new Drug("As", "Drug1"));
		String estadoInicial = "F";

		// action
		String estadoFinal = rule.processState(drugs, estadoInicial);

		// Verificaçtion
		assertTrue(estadoFinal.compareTo("H") == 0);
	}

}
