package hospital.simulator;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import com.hospital.simulator.entities.Drug;
import com.hospital.simulator.rules.beans.AntibioticCuresTuberculosis;
import com.hospital.simulator.rules.beans.ParacetamolMoreAspirin;

public class InsulinMoreParacetamolTest {
	
	@Test
	public void insulinParecetamorDieTest(){
		
		// scene
		ParacetamolMoreAspirin rule = new ParacetamolMoreAspirin();
		List<Drug> drugs = new ArrayList<Drug>();
		drugs.add(new Drug("As", "Drug1"));
		drugs.add(new Drug("P", "Drug2"));
		String estadoInicial = "F";

		// action
		String estadoFinal = rule.processState(drugs, estadoInicial);

		// Verificaçtion
		assertTrue(estadoFinal.compareTo("X") == 0);

	}

}
