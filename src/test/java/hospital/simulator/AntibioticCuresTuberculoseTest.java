package hospital.simulator;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import com.hospital.simulator.entities.Drug;
import com.hospital.simulator.rules.beans.AntibioticCuresTuberculosis;

public class AntibioticCuresTuberculoseTest {

	@Test
	public void tuberculosCureTest() {

		// scene
		AntibioticCuresTuberculosis rule = new AntibioticCuresTuberculosis();
		List<Drug> drugs = new ArrayList<Drug>();
		drugs.add(new Drug("An", "Drug1"));
		String estadoInicial = "T";

		// action
		String estadoFinal = rule.processState(drugs, estadoInicial);

		// Verificaçtion
		assertTrue(estadoFinal.compareTo("H") == 0);
	}

}
