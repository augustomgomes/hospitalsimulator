package hospital.simulator;

import hospital.simulator.process.management.SimulatorManager;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        System.out.println( "Hello from hospital Simulator!" );
        String one = args[0]; 
        String two = args[1];
        System.out.println( "Primeiro parametro "+ one);
        System.out.println( "Segundo parametro "+ two);
     
       // System.out.println( args[0] );
       // System.out.println( args[1] );
        SimulatorManager instance = SimulatorManager.getInstance();
        instance.initApp(args);
        
    }
}
