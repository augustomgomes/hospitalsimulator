package hospital.simulator.rules;

import java.util.ArrayList;
import java.util.List;

import hospital.simulator.entities.Drug;
import hospital.simulator.rules.beans.AntibioticCuresTuberculosis;
import hospital.simulator.rules.beans.AspirinCuresFever;
import hospital.simulator.rules.beans.FlyingSpaghetiMonster;
import hospital.simulator.rules.beans.InsulinPreventDiabeticsD;
import hospital.simulator.rules.beans.ParacetamolCuresFever;

public class RulesManager {

	private List<RuleFacade> rulesList;
	private List<String> drugsInput;

	public RulesManager() {

	}

	public RulesManager(List<String> drugsList) {
		this.drugsInput = drugsList;
		this.rulesList = new ArrayList<RuleFacade>();
		initRules();

	}

	private void initRules() {

		RuleFacade rule = new AntibioticCuresTuberculosis();
		this.rulesList.add(rule);

		rule = new AspirinCuresFever();
		this.rulesList.add(rule);

		this.rulesList.add(new InsulinPreventDiabeticsD());
		this.rulesList.add(new ParacetamolCuresFever());
		this.rulesList.add(new FlyingSpaghetiMonster());

	}

	public List<RuleFacade> getRulesList() {
		return rulesList;
	}

}
