package hospital.simulator.rules.beans;

import java.util.List;
import java.util.Optional;

import hospital.simulator.entities.Drug;
import hospital.simulator.rules.RuleFacade;
import hospital.simulator.types.DrugsType;
import hospital.simulator.types.PatientState;

public class AspirinCuresFever implements RuleFacade{
	
	public AspirinCuresFever() {
		
	}
	
	public Optional<Drug> findDrugById(final List<Drug> list, final String Id) {
	    return list.stream().filter(d -> d.getId().equals(Id)).findAny();
	}

	public String processState(List<Drug> drugs, String initialState) {
		
		if(findDrugById(drugs, DrugsType.As.toString()).isPresent())			
			if(findDrugById(drugs, DrugsType.P.toString()).isPresent())
				return "X";
				
				//System.out.println( "Esta presente");
				
		//		return "X";
		
		/*
		Drug drug1 =  new Drug(DrugsType.AS.toString(), "Aspirin");
		Drug drug2 =  new Drug(DrugsType.P.toString(), "Paracetamol");
		
		
		
		if (drugs.contains(drug1))
			if(drugs.contains(drug2))
				return "X";
		*/
		
		for(Drug drug:drugs)
			if (drug.getId().equals(DrugsType.As.toString()))
				if(initialState.equals(PatientState.F.toString()))
					return "H";
		// return unchanged state
		return initialState;
	}

}
