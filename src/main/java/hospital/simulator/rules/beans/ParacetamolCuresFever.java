package hospital.simulator.rules.beans;

import java.util.List;
import java.util.Optional;

import hospital.simulator.entities.Drug;
import hospital.simulator.process.management.SimulatorManager;
import hospital.simulator.rules.RuleFacade;
import hospital.simulator.types.DrugsType;
import hospital.simulator.types.PatientState;

public class ParacetamolCuresFever implements RuleFacade{
	
	public ParacetamolCuresFever() {
		
	}
	
	public Optional<Drug> findDrugById(final List<Drug> list, final String Id) {
	    return list.stream().filter(d -> d.getId().equals(Id)).findAny();
	}

	public String processState(List<Drug> drugs, String initialState) {
		/*Drug drug1 =  new Drug(DrugsType.As.toString(), "Aspirin");
		Drug drug2 =  new Drug(DrugsType.P.toString(), "Paracetamol");
		if (drugs.contains(drug1))
			if(drugs.contains(drug2))
				return "X";
		*/
		if(findDrugById(drugs, DrugsType.As.toString()).isPresent())			
			if(findDrugById(drugs, DrugsType.P.toString()).isPresent())
				return "X";
		
		for(Drug drug:drugs)
			if (drug.getId().equals(DrugsType.As.toString()))
				if(initialState.equals(PatientState.F.toString()))
					return "H";
		// return unchanged state
		return initialState;
	}

}
