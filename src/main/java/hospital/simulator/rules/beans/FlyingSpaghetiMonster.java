package hospital.simulator.rules.beans;

import java.util.List;

import hospital.simulator.entities.Drug;
import hospital.simulator.process.management.SimulatorManager;
import hospital.simulator.rules.RuleFacade;
import hospital.simulator.types.DrugsType;
import hospital.simulator.types.PatientState;

public class FlyingSpaghetiMonster implements RuleFacade {

	public FlyingSpaghetiMonster() {

	}

	public String processState(List<Drug> drugs, String initialState) {
		
		SimulatorManager instance = SimulatorManager.getInstance();
		
		//aqui deveria pesquisar aleatoriamente um paciente morto
		if (instance.getNumeroPacientesTratados() > 1000000)			
				if (initialState.equals(PatientState.X.toString())) {					
					instance.setNumeroPacientesTratados(0);
					return "H";
				}
					

		// return unchanged state
		return initialState;
	}

}
