package hospital.simulator.rules.beans;

import java.util.List;

import hospital.simulator.entities.Drug;
import hospital.simulator.rules.RuleFacade;
import hospital.simulator.types.DrugsType;
import hospital.simulator.types.PatientState;

public class AntibioticCuresTuberculosis implements RuleFacade {

	public AntibioticCuresTuberculosis() {

	}

	public String processState(List<Drug> drugs, String initialState) {

		for (Drug drug : drugs)
			if (drug.getId().equals(DrugsType.An.toString()))
				if (initialState.equals(PatientState.H.toString()))
					return "D";

		// return unchanged state
		return initialState;
	}

}
