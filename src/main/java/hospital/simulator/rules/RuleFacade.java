package hospital.simulator.rules;

import java.util.List;

import hospital.simulator.entities.Drug;

public interface RuleFacade {

	String processState(List<Drug> drugs, String initialState);

}
