package hospital.simulator.entities;

public class Drug {

	private String Id;
	private String name;

	public Drug() {

	}

	public Drug(String Id, String name) {
		this.Id = Id;
		this.name = name;
	}

	public String getId() {
		return Id;
	}

	public void setId(String id) {
		Id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
