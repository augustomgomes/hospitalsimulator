package hospital.simulator.process.management;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import hospital.simulator.entities.Drug;
import hospital.simulator.rules.RuleFacade;
import hospital.simulator.rules.RulesManager;

public class PatientsTreatmentsManager {

	List<String> patientsInputStateList;
	List<String> patientsFinalStateList;
	List<Drug> drugsInputList;

	RulesManager rulesManager;

	public PatientsTreatmentsManager() {

	}

	// contrutor que converte string em lista de strings
	public PatientsTreatmentsManager(String PatientsEntry, String DrugsList) {
		List<String> patientsList = Arrays.asList(PatientsEntry.split(","));
		List<String> drugsList = Arrays.asList(DrugsList.split(","));
		
		this.patientsFinalStateList = new ArrayList<String>();

		this.patientsInputStateList = patientsList;
		
		this.drugsInputList = new ArrayList<Drug>();
		
		for(String s: drugsList)
		{
			this.drugsInputList.add(new Drug(s, "Drug" +"-"+s));
		}
			
	//	this.drugsInputList = drugsList;

		// RulesManager rManager = new RulesManager(this.drugsInputList);

		this.rulesManager = new RulesManager(drugsList);

	}

	public void processPacientsTreatments() {
		System.out.println("Lista de Estados Pacientes");
		for (String s : this.patientsInputStateList)
			System.out.println(s);

		System.out.println("Lista de medicamentos");
		for (Drug s2 : this.drugsInputList)
			System.out.println(s2);

		List<RuleFacade> rulesList = this.rulesManager.getRulesList();

		for (String estadoPaciente : this.patientsInputStateList) {

			for (RuleFacade rule : rulesList) {
				String Estadofinal = rule.processState(this.drugsInputList, estadoPaciente);
				estadoPaciente = Estadofinal;
			}
			
			this.patientsFinalStateList.add(estadoPaciente);
			
		}
		
		
		printResult();

	}
	
	
	private void printResult() {
		 System.out.println("Resultado Final");
		for(String s :this.patientsFinalStateList) 
			 System.out.println( "estadoFinalPaciente: "+ s);
	}

}
