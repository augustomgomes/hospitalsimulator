package hospital.simulator.process.management;
	


public class SimulatorManager {
	private static SimulatorManager instance;
	
	private static long numeroPacientesTratados;
	
	
	private SimulatorManager() {
		
	}
	public static SimulatorManager getInstance() {
		
		
		if(instance == null)
			instance = new SimulatorManager();
		
		return instance;
	}
	
	public static void initApp(String[] args) {
		PatientsTreatmentsManager treatmentsmainProcess = new PatientsTreatmentsManager(args[0] , args[1] );
		treatmentsmainProcess.processPacientsTreatments();
	}
	
	public static long getNumeroPacientesTratados() {
		return numeroPacientesTratados;
	}
	public static void setNumeroPacientesTratados(long numeroPacientesTratados) {
		SimulatorManager.numeroPacientesTratados = numeroPacientesTratados;
	}
	
	
	
	

}
