package hospital.simulator.types;

public enum PatientState {
	F, // Fever
	H, // Healty
	D, // Diabetes
	T, // Tubercusose
	X // Dead
}
