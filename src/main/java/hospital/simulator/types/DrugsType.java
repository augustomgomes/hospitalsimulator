package hospital.simulator.types;

public enum DrugsType {
	As, //Aspirin
	An, //Antibiotic
	I, //Insulin
	P //Paracetamol
}
