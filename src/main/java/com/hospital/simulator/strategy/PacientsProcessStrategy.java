package com.hospital.simulator.strategy;

//strategy pattern allow to select a strategy to run the rules.
public interface PacientsProcessStrategy {
	
	public void processPacientsTreatments();

}
