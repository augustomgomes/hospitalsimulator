package com.hospital.simulator.process.management;

import java.io.IOException;
import java.util.Properties;

import com.hospital.simulator.strategy.PacientsProcessStrategy;

//Singleton to avoid more than one main class.
public class SimulatorManager {
	private static SimulatorManager instance;
	
	private static long numeroPacientesTratados;	
	
	private SimulatorManager() {
		
	}
	
	public static SimulatorManager getInstance() {
		
		
		if(instance == null)
			instance = new SimulatorManager();
		
		return instance;
	}
	
	public static void initApp(String[] args) {
		
		//read properties file for strategy selection
		Properties prop = new Properties();
		try {			
			prop.load(SimulatorManager.class.getResourceAsStream("/simulator.properties"));			
		} catch (IOException e) {
			
			e.printStackTrace();
		}
		
		String propertie = prop.getProperty("rules.inputState.unchangable");	
		Boolean inputStateUnchanble = Boolean.valueOf(propertie);
		PacientsProcessStrategy strategy= null ;
		
		if(!inputStateUnchanble)
			 strategy = new PatientsProcessChangableStatesImplementation(args[0] , args[1] );
		else
			strategy = new PatientsProcessUnChangableStatesImplementation(args[0] , args[1] );
		
		strategy.processPacientsTreatments();
	}
	
	public static long getNumeroPacientesTratados() {
		return numeroPacientesTratados;
	}
	public static void setNumeroPacientesTratados(long numeroPacientesTratados) {
		SimulatorManager.numeroPacientesTratados = numeroPacientesTratados;
	}	
	
}
