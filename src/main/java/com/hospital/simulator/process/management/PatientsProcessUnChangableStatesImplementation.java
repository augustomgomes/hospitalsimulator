package com.hospital.simulator.process.management;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.hospital.simulator.entities.Drug;
import com.hospital.simulator.rules.RuleFacade;
import com.hospital.simulator.rules.RulesManager;
import com.hospital.simulator.strategy.PacientsProcessStrategy;

public class PatientsProcessUnChangableStatesImplementation implements PacientsProcessStrategy{

	List<String> patientsInputStateList;
	List<String> patientsFinalStateList;
	List<Drug> drugsInputList;

	RulesManager rulesManager;

	public PatientsProcessUnChangableStatesImplementation() {

	}

	// construtor that convert string in list of strings
	public PatientsProcessUnChangableStatesImplementation(String PatientsEntry, String DrugsList) {
		List<String> patientsList = Arrays.asList(PatientsEntry.split(","));
		List<String> drugsList = Arrays.asList(DrugsList.split(","));
		
		this.patientsFinalStateList = new ArrayList<String>();

		this.patientsInputStateList = patientsList;
		
		this.drugsInputList = new ArrayList<Drug>();
		
		for(String s: drugsList)
		{
			this.drugsInputList.add(new Drug(s, "Drug" +"-"+s));
		}		

		this.rulesManager = new RulesManager(drugsList);

	}

	public void processPacientsTreatments() {	

		List<RuleFacade> rulesList = this.rulesManager.getAllRulesList();

		for (String estadoPaciente : this.patientsInputStateList) {
			String Estadofinal = "";
			StringBuilder stBuilder = new StringBuilder();
			
			
			for (RuleFacade rule : rulesList) {
				 Estadofinal = rule.processState(this.drugsInputList, estadoPaciente);
				 if(Estadofinal.compareTo(estadoPaciente) != 0)
				 {
					 stBuilder.delete(0, Estadofinal.length());
					 stBuilder.append(Estadofinal);
				 }				
			}
			
			this.patientsFinalStateList.add(stBuilder.toString());			
		}				
		printResult();				
	}
	
	
	
	private void printResult() {	
		
		 long numF = this.patientsFinalStateList.stream()
                 .filter(x -> x.contains("F"))
                 .count();
		
		 long numH = this.patientsFinalStateList.stream()
                 .filter(x -> x.contains("H"))
                 .count();
		 long numD = this.patientsFinalStateList.stream()
                 .filter(x -> x.contains("D"))
                 .count();
		 
		 long numT = this.patientsFinalStateList.stream()
                 .filter(x -> x.contains("T"))
                 .count();
		 
		 long numX = this.patientsFinalStateList.stream()
                 .filter(x -> x.contains("X"))
                 .count();
		 
		 System.out.print("F:");
		 System.out.print(numF);
		 System.out.print(",H:");
		 System.out.print(numH);
		 System.out.print(",D:");
		 System.out.print(numD);
		 System.out.print(",T:");
		 System.out.print(numT);
		 System.out.print(",X:");
		 System.out.print(numX);		 
	}

}
