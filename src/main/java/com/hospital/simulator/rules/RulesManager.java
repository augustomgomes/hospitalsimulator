package com.hospital.simulator.rules;

import java.util.ArrayList;
import java.util.List;

import com.hospital.simulator.entities.Drug;
import com.hospital.simulator.rules.beans.AntibioticCuresTuberculosis;
import com.hospital.simulator.rules.beans.AspirinCuresFever;
import com.hospital.simulator.rules.beans.FlyingSpaghetiMonster;
import com.hospital.simulator.rules.beans.InsulinMoreAntibiotic;
import com.hospital.simulator.rules.beans.InsulinPreventDiabeticsD;
import com.hospital.simulator.rules.beans.ParacetamolCuresFever;
import com.hospital.simulator.rules.beans.ParacetamolMoreAspirin;


// this class is a point of access to all rules
public class RulesManager {

	private List<RuleFacade> rulesList;
	private List<String> drugsInput;

	public RulesManager() {

	}

	public RulesManager(List<String> drugsList) {
		this.drugsInput = drugsList;
		this.rulesList = new ArrayList<RuleFacade>();
		initRules();

	}

	private void initRules() {
		
		this.rulesList.add(new AntibioticCuresTuberculosis());
		this.rulesList.add(new AspirinCuresFever());
		this.rulesList.add(new InsulinPreventDiabeticsD());
		this.rulesList.add(new ParacetamolCuresFever());
		this.rulesList.add(new InsulinMoreAntibiotic());
		this.rulesList.add(new ParacetamolMoreAspirin());		
		this.rulesList.add(new FlyingSpaghetiMonster());

	}

	public List<RuleFacade> getAllRulesList() {
		return rulesList;
	}

}
