package com.hospital.simulator.rules.beans;

import java.util.List;

import com.hospital.simulator.entities.Drug;
import com.hospital.simulator.process.management.SimulatorManager;
import com.hospital.simulator.rules.RuleFacade;
import com.hospital.simulator.types.DrugsType;
import com.hospital.simulator.types.PatientState;

public class FlyingSpaghetiMonster implements RuleFacade {

	public FlyingSpaghetiMonster() {

	}

	public String processState(List<Drug> drugs, String initialState) {
		
		SimulatorManager instance = SimulatorManager.getInstance();
		
		//aqui deveria pesquisar aleatoriamente um paciente morto
		if (instance.getNumeroPacientesTratados() > 1000000)			
				if (initialState.equals(PatientState.X.toString())) {					
					instance.setNumeroPacientesTratados(0);
					return "H";
				}
					

		// return unchanged state
		return initialState;
	}

}
