package com.hospital.simulator.rules.beans;

import java.util.List;
import java.util.Optional;

import com.hospital.simulator.entities.Drug;
import com.hospital.simulator.rules.RuleFacade;
import com.hospital.simulator.types.DrugsType;
import com.hospital.simulator.types.PatientState;

public class AspirinCuresFever implements RuleFacade{
	
	public AspirinCuresFever() {
		
	}
	
	public Optional<Drug> findDrugById(final List<Drug> list, final String Id) {
	    return list.stream().filter(d -> d.getId().equals(Id)).findAny();
	}

	public String processState(List<Drug> drugs, String initialState) {
		
//		if(findDrugById(drugs, DrugsType.As.toString()).isPresent())			
//			if(findDrugById(drugs, DrugsType.P.toString()).isPresent())
//				return "X";			
			
		
		for(Drug drug:drugs)
			if (drug.getId().equals(DrugsType.As.toString()))
				if(initialState.equals(PatientState.F.toString()))
					return "H";
		// return unchanged state
		return initialState;
	}

}
