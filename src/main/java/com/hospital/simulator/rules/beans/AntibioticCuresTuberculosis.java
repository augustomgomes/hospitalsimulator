package com.hospital.simulator.rules.beans;

import java.util.List;

import com.hospital.simulator.entities.Drug;
import com.hospital.simulator.rules.RuleFacade;
import com.hospital.simulator.types.DrugsType;
import com.hospital.simulator.types.PatientState;

public class AntibioticCuresTuberculosis implements RuleFacade {

	public AntibioticCuresTuberculosis() {

	}

	public String processState(List<Drug> drugs, String initialState) {

		for (Drug drug : drugs)
			if (drug.getId().equals(DrugsType.An.toString()))
				if (initialState.equals(PatientState.T.toString()))
					return "H";

		// return unchanged state
		return initialState;
	}

}
