package com.hospital.simulator.rules.beans;

import java.util.List;

import com.hospital.simulator.entities.Drug;
import com.hospital.simulator.rules.RuleFacade;
import com.hospital.simulator.types.DrugsType;
import com.hospital.simulator.types.PatientState;

public class InsulinPreventDiabeticsD implements RuleFacade {

	public InsulinPreventDiabeticsD() {

	}

	public String processState(List<Drug> drugs, String initialState) {

		for (Drug drug : drugs)
			if (drug.getId().equals(DrugsType.I.toString()))
				if (initialState.equals(PatientState.D.toString()))
					return "D";

		// return unchanged state
		return initialState;
	}

}
