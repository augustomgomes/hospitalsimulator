package com.hospital.simulator.rules.beans;

import java.util.List;
import java.util.Optional;

import com.hospital.simulator.entities.Drug;
import com.hospital.simulator.rules.RuleFacade;
import com.hospital.simulator.types.DrugsType;
import com.hospital.simulator.types.PatientState;

public class InsulinMoreAntibiotic implements RuleFacade {

	public InsulinMoreAntibiotic() {

	}

	public Optional<Drug> findDrugById(final List<Drug> list, final String Id) {
		return list.stream().filter(d -> d.getId().equals(Id)).findAny();
	}

	public String processState(List<Drug> drugs, String initialState) {
		if (findDrugById(drugs, DrugsType.I.toString()).isPresent())
			if (findDrugById(drugs, DrugsType.An.toString()).isPresent())
				if(initialState.equals(PatientState.H.toString()))
					return "F";

		return initialState;
	}

}
