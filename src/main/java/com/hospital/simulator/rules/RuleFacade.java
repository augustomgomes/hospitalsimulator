package com.hospital.simulator.rules;

import java.util.List;

import com.hospital.simulator.entities.Drug;

// Facade Pattern allow abstraction of the way each rule is implemented
public interface RuleFacade {

	String processState(List<Drug> drugs, String initialState);

}
